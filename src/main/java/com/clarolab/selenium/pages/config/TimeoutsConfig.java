package com.clarolab.selenium.pages.config;

import com.google.common.base.Preconditions;

import java.util.concurrent.TimeUnit;

public final class TimeoutsConfig {

    // Standard timeouts for most common usages
    private final int clickTimeoutSeconds;
    private final int webElementPresenceTimeoutSeconds;
    private final int pollingWithRefreshTimeoutSeconds;
    private final int pageRefreshTimeoutSeconds;

    // Arbitrary timeouts configured by client
    private final int shortTimeoutSeconds;
    private final int mediumTimeoutSeconds;
    private final int longTimeoutSeconds;

    // Pauses when polling or entering keys
    private final int pauseBetweenKeysMillis;
    private final int pauseBetweenTriesMillis;
    private final int pauseBetweenRefreshSeconds;

    // Timeouts used for configuring the underlying WebDriver
    private final int pageLoadTimeoutSeconds;
    private final int pageReadyTimeoutSeconds;
    private final int implicitWaitTimeoutMillis;

    public static Builder builder() {
        return new Builder();
    }

    public static TimeoutsConfig defaultTimeoutsConfig() {
        return new Builder().build();
    }

    private TimeoutsConfig(int clickTimeoutSeconds, int webElementPresenceTimeoutSeconds, int pollingWithRefreshTimeoutSeconds,
                           int pageRefreshTimeoutSeconds, int shortTimeoutSeconds, int mediumTimeoutSeconds, int longTimeoutSeconds,
                           int pauseBetweenKeysMillis, int pauseBetweenTriesMillis, int pauseBetweenRefreshSeconds,
                           int pageLoadTimeoutSeconds, int pageReadyTimeoutSeconds, int implicitWaitTimeoutMillis) {
        this.clickTimeoutSeconds = clickTimeoutSeconds;
        this.webElementPresenceTimeoutSeconds = webElementPresenceTimeoutSeconds;
        this.pollingWithRefreshTimeoutSeconds = pollingWithRefreshTimeoutSeconds;
        this.pageRefreshTimeoutSeconds = pageRefreshTimeoutSeconds;
        this.shortTimeoutSeconds = shortTimeoutSeconds;
        this.mediumTimeoutSeconds = mediumTimeoutSeconds;
        this.longTimeoutSeconds = longTimeoutSeconds;
        this.pauseBetweenKeysMillis = pauseBetweenKeysMillis;
        this.pauseBetweenTriesMillis = pauseBetweenTriesMillis;
        this.pauseBetweenRefreshSeconds = pauseBetweenRefreshSeconds;
        this.pageLoadTimeoutSeconds = pageLoadTimeoutSeconds;
        this.pageReadyTimeoutSeconds = pageReadyTimeoutSeconds;
        this.implicitWaitTimeoutMillis = implicitWaitTimeoutMillis;
    }

    public int getTimeoutInSeconds(TimeoutType timeout) {
        Preconditions.checkNotNull(timeout, "Cannot get timeout for null timeout.");
        Preconditions.checkArgument(timeout != TimeoutType.DEFAULT, "Can only get the standard timeout for timeout types other than DEFAULT");
        switch (timeout) {

            case CLICK_TIMEOUT:
                return getClickTimeoutSeconds();
            case WEB_ELEMENT_PRESENCE_TIMEOUT:
                return getWebElementPresenceTimeoutSeconds();
            case POLLING_WITH_REFRESH_TIMEOUT:
                return getPollingWithRefreshTimeoutSeconds();
            case PAGE_REFRESH_TIMEOUT:
                return getPageRefreshTimeoutSeconds();
            case PAGE_LOAD_TIMEOUT:
                return getPageLoadTimeoutSeconds();
            case PAGE_READY_TIMEOUT:
                return getPageReadyTimeoutSeconds();
            case SHORT:
                return getShortTimeoutSeconds();
            case MEDIUM:
                return getMediumTimeoutSeconds();
            case LONG:
                return getLongTimeoutSeconds();
            case ONE_SECOND:
                return 1;
            case TWO_SECONDS:
                return 2;
            case FIVE_SECONDS:
                return 5;
            case TEN_SECONDS:
                return 10;
            case TWENTY_SECONDS:
                return 20;
            case SIXTY_SECONDS:
                return 60;
            case NINETY_SECONDS:
                return 90;
            case TWO_MINUTES:
                return (int) TimeUnit.MINUTES.toSeconds(2);
            case THREE_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(3);
            case FIVE_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(5);
            case TEN_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(10);
            case THIRTY_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(30);
            case SIXTY_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(60);
            case NINETY_MINUTES:
                return (int)TimeUnit.MINUTES.toSeconds(90);
            case TWO_HOURS:
                return (int)TimeUnit.HOURS.toSeconds(2);
            default:
                return getWebElementPresenceTimeoutSeconds();
        }
    }

    public int getClickTimeoutSeconds() {
        return clickTimeoutSeconds;
    }

    public int getWebElementPresenceTimeoutSeconds() {
        return webElementPresenceTimeoutSeconds;
    }

    public int getPollingWithRefreshTimeoutSeconds() {
        return pollingWithRefreshTimeoutSeconds;
    }

    public int getPageRefreshTimeoutSeconds() {
        return pageRefreshTimeoutSeconds;
    }

    public int getShortTimeoutSeconds() {
        return shortTimeoutSeconds;
    }

    public int getMediumTimeoutSeconds() {
        return mediumTimeoutSeconds;
    }

    public int getLongTimeoutSeconds() {
        return longTimeoutSeconds;
    }

    public int getPauseBetweenKeysMillis() {
        return pauseBetweenKeysMillis;
    }

    public int getPauseBetweenTriesMillis() {
        return pauseBetweenTriesMillis;
    }

    public int getPauseBetweenRefreshSeconds() {
        return pauseBetweenRefreshSeconds;
    }

    public int getPageLoadTimeoutSeconds() {
        return pageLoadTimeoutSeconds;
    }

    public int getPageReadyTimeoutSeconds() {
        return pageReadyTimeoutSeconds;
    }

    public int getImplicitWaitTimeoutMillis() {
        return implicitWaitTimeoutMillis;
    }

    public static final class Builder {
        public Builder() {
            this.clickTimeoutSeconds = DefaultTimeouts.CLICK_TIMEOUT_SECONDS;
            this.webElementPresenceTimeoutSeconds = DefaultTimeouts.PRESENCE_TIMEOUT_SECONDS;
            this.pollingWithRefreshTimeoutSeconds = DefaultTimeouts.POLLING_WITH_REFRESH_TIMEOUT_SECONDS;
            this.pageRefreshTimeoutSeconds = DefaultTimeouts.REFRESH_TIMEOUT_SECONDS;
            this.shortTimeoutSeconds = DefaultTimeouts.SHORT_TIMEOUT_SECONDS;
            this.mediumTimeoutSeconds = DefaultTimeouts.MEDIUM_TIMEOUT_SECONDS;
            this.longTimeoutSeconds = DefaultTimeouts.LONG_TIMEOUT_SECONDS;
            this.pauseBetweenKeysMillis = DefaultTimeouts.PAUSE_BETWEEN_KEYS_MILLIS;
            this.pauseBetweenTriesMillis = DefaultTimeouts.PAUSE_BETWEEN_TRIES_MILLIS;
            this.pauseBetweenRefreshSeconds = DefaultTimeouts.PAUSE_BETWEEN_REFRESH_SECONDS;
            this.pageLoadTimeoutSeconds = DefaultTimeouts.PAGE_LOAD_TIMEOUT_SECONDS;
            this.pageReadyTimeoutSeconds = DefaultTimeouts.PAGE_READY_TIMEOUT_SECONDS;
            this.implicitWaitTimeoutMillis = DefaultTimeouts.IMPLICIT_WAIT_TIMEOUT_MILLIS;
        }

        public TimeoutsConfig build() {
            return new TimeoutsConfig(clickTimeoutSeconds,
                    webElementPresenceTimeoutSeconds,
                    pollingWithRefreshTimeoutSeconds,
                    pageRefreshTimeoutSeconds,
                    shortTimeoutSeconds,
                    mediumTimeoutSeconds,
                    longTimeoutSeconds,
                    pauseBetweenKeysMillis,
                    pauseBetweenTriesMillis,
                    pauseBetweenRefreshSeconds,
                    pageLoadTimeoutSeconds,
                    pageReadyTimeoutSeconds,
                    implicitWaitTimeoutMillis);
        }

        public Builder clickTimeoutSeconds(int clickTimeoutSeconds) {
            this.clickTimeoutSeconds = clickTimeoutSeconds;
            return this;
        }

        public Builder webElementPresenceTimeoutSeconds(int webElementPresenceTimeoutSeconds) {
            this.webElementPresenceTimeoutSeconds = webElementPresenceTimeoutSeconds;
            return this;
        }

        public Builder pollingWithRefreshTimeoutSeconds(int pollingWithRefreshTimeoutSeconds) {
            this.pollingWithRefreshTimeoutSeconds = pollingWithRefreshTimeoutSeconds;
            return this;
        }

        public Builder pageRefreshTimeoutSeconds(int pageRefreshTimeoutSeconds) {
            this.pageRefreshTimeoutSeconds = pageRefreshTimeoutSeconds;
            return this;
        }

        public Builder shortTimeoutSeconds(int shortTimeoutSeconds) {
            this.shortTimeoutSeconds = shortTimeoutSeconds;
            return this;
        }

        public Builder mediumTimeoutSeconds(int mediumTimeoutSeconds) {
            this.mediumTimeoutSeconds = mediumTimeoutSeconds;
            return this;
        }

        public Builder longTimeoutSeconds(int longTimeoutSeconds) {
            this.longTimeoutSeconds = longTimeoutSeconds;
            return this;
        }

        public Builder pauseBetweenKeysMillis(int pauseBetweenKeysMillis) {
            this.pauseBetweenKeysMillis = pauseBetweenKeysMillis;
            return this;
        }

        public Builder pauseBetweenTriesMillis(int pauseBetweenTriesMillis) {
            this.pauseBetweenTriesMillis = pauseBetweenTriesMillis;
            return this;
        }

        public Builder pauseBetweenRefreshSeconds(int pauseBetweenRefreshSeconds) {
            this.pauseBetweenRefreshSeconds = pauseBetweenRefreshSeconds;
            return this;
        }

        public Builder pageLoadTimoutSeconds(int pageLoadTimeoutSeconds) {
            this.pageLoadTimeoutSeconds = pageLoadTimeoutSeconds;
            return this;
        }

        public Builder pageReadyTimoutSeconds(int pageReadyTimeoutSeconds) {
            this.pageReadyTimeoutSeconds = pageReadyTimeoutSeconds;
            return this;
        }

        public Builder implicitWaitTimeoutMillis(int implicitWaitTimeoutMillis) {
            this.implicitWaitTimeoutMillis = implicitWaitTimeoutMillis;
            return this;
        }

        // Standard timeouts for most common usages, all in seconds
        private int clickTimeoutSeconds;
        private int webElementPresenceTimeoutSeconds;
        private int pollingWithRefreshTimeoutSeconds;
        private int pageRefreshTimeoutSeconds;

        // Arbitrary timeouts configured by client, all in seconds
        private int shortTimeoutSeconds;
        private int mediumTimeoutSeconds;
        private int longTimeoutSeconds;

        // Pauses when polling or entering keys
        private int pauseBetweenKeysMillis;
        private int pauseBetweenTriesMillis;
        private int pauseBetweenRefreshSeconds;

        // Timeouts used for configuring the underlying WebDriver
        private int pageLoadTimeoutSeconds;
        private int pageReadyTimeoutSeconds;
        private int implicitWaitTimeoutMillis;
    }
}
