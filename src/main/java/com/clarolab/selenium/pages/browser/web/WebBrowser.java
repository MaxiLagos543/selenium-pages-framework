package com.clarolab.selenium.pages.browser.web;

import com.clarolab.selenium.pages.browser.Browser;
import com.clarolab.selenium.pages.config.TimeoutsConfig;
import com.clarolab.selenium.pages.exception.FrameworkWebDriverException;
import com.clarolab.selenium.pages.pages.BaseTopLevelPage;
import com.clarolab.selenium.pages.pages.TopLevelPage;
import com.clarolab.selenium.pages.webservice.EndpointBuilder;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public abstract class WebBrowser extends Browser<WebDriver> {
    private static final Logger logger = LoggerFactory.getLogger(WebBrowser.class);

    private final Optional<String> webDriverPath;
    private final Optional<String> browserBinaryPath;
    private final Optional<String> browserVersion;
    private final Optional<String> browserLocale;
    private final Optional<Integer> startWindowWidth;
    private final Optional<Integer> startWindowHeight;
    private final Optional<Level> browserLogLevel;
    private final Optional<String> browserLogFile;
    private final Optional<Platform> platform;

    public WebBrowser(String baseTestUrl,
                      TimeoutsConfig timeouts,
                      Optional<String> webDriverPath,
                      Optional<String> browserBinaryPath,
                      Optional<String> browserVersion,
                      Optional<String> browserLocale,
                      Optional<Integer> startWindowWidth,
                      Optional<Integer> startWindowHeight,
                      Optional<Platform> platform) {

        this(baseTestUrl, timeouts, webDriverPath, browserBinaryPath, browserVersion, browserLocale,
                startWindowWidth, startWindowHeight,
                Optional.empty(), Optional.empty(), platform);

    }

    public WebBrowser(String baseTestUrl,
                      TimeoutsConfig timeouts,
                      Optional<String> webDriverPath,
                      Optional<String> browserBinaryPath,
                      Optional<String> browserVersion,
                      Optional<String> browserLocale,
                      Optional<Integer> startWindowWidth,
                      Optional<Integer> startWindowHeight,
                      Optional<Level> browserLogLevel,
                      Optional<String> browserLogFile,
                      Optional<Platform> platform) {
        super(baseTestUrl, timeouts);
        this.webDriverPath = webDriverPath;
        this.browserBinaryPath = browserBinaryPath;
        this.browserVersion = browserVersion;
        this.browserLocale = browserLocale;
        this.startWindowWidth = startWindowWidth;
        this.startWindowHeight = startWindowHeight;
        this.browserLogLevel = browserLogLevel;
        this.browserLogFile = browserLogFile;
        this.platform = platform;
    }

    public void initializeBrowser() throws FrameworkWebDriverException {
        this.webDriver = createWebDriver();
        if (startWindowWidth.isPresent() && startWindowHeight.isPresent()) {
            this.webDriver.manage().window().setSize(new Dimension(startWindowWidth.get(), startWindowHeight.get()));
        }
        // Safari web driver doesn't support setting timeouts.
        if (getBrowserType() != WebBrowserType.SAFARI) {
            this.webDriver.manage().timeouts().pageLoadTimeout(getPageTimeoutSeconds(), TimeUnit.SECONDS);
            this.webDriver.manage().timeouts().implicitlyWait(getImplicitWaitTimeoutMillis(), TimeUnit.MILLISECONDS);
        }
        logger.info("SUCCESS - Created WebBrowser of type {}: {}", getBrowserType(), webDriver);
    }

    public abstract WebBrowserType getBrowserType();

    public abstract DesiredCapabilities getDesiredCapabilities();

    public LoggingPreferences getLoggingPreferences() {
        Level level = getLogLevel();
        LoggingPreferences loggingPreferences = new LoggingPreferences();
        loggingPreferences.enable(LogType.BROWSER, level);
        loggingPreferences.enable(LogType.CLIENT, level);
        loggingPreferences.enable(LogType.DRIVER, level);
        loggingPreferences.enable(LogType.SERVER, level);
        return loggingPreferences;
    }

    public Optional<Integer> getStartWindowWidth() {
        return startWindowWidth;
    }

    public Optional<Integer> getStartWindowHeight() {
        return startWindowHeight;
    }

    public Optional<String> getWebDriverPath() {
        return webDriverPath;
    }

    public Optional<String> getBrowserBinaryPath() {
        return browserBinaryPath;
    }

    public Optional<String> getBrowserVersion() {
        return browserVersion;
    }

    public Optional<String> getBrowserLocale() {
        return browserLocale;
    }

    public Optional<Level> getBrowserLogLevel() {
        return browserLogLevel;
    }

    public Level getLogLevel() {
        return browserLogLevel.isPresent() ? browserLogLevel.get() : Level.WARNING;
    }

    public Optional<String> getBrowserLogFile() {
        return browserLogFile;
    }

    public Optional<Platform> getPlatform() {
        return platform;
    }

    public TimeoutsConfig getTimeouts() {
        return timeouts;
    }

    public TopLevelPage openPageByURL(String href) {
        return openPageByURL(href, BaseTopLevelPage.class);
    }

    public <T extends TopLevelPage> T openPageByURL(URI uri, Class<T> pageClass) {
        URI absoluteURI;
        if (uri.isAbsolute()) {
            absoluteURI = uri;
        } else {
            String fullURIStr = EndpointBuilder.uri(baseTestUrl, "/", uri.toString());
            absoluteURI = URI.create(fullURIStr);
        }
        logger.info("Opening web page by URL {}", absoluteURI);
        runLeavePageHook();
        invalidateCachedPage();
        T page = PAGE_UTILS.loadPageFromURL(absoluteURI, pageClass, getWebDriver(), getActions());
        setCachedPage(page);
        return page;
    }

    public <T extends TopLevelPage> T openPageByURL(String href, Class<T> pageClass) {
        URI uri = URI.create(href);
        return openPageByURL(uri, pageClass);
    }

    public void refreshPage() {
        runLeavePageHook();
        webDriver.navigate().refresh();
        if (optionalCachedPage.isPresent()) {
            TopLevelPage cachedPage = optionalCachedPage.get().getCachedPage();
            cachedPage.refreshElements();
        }
    }

    public <T extends TopLevelPage> T refreshPage(Class<T> pageClass) {
        runLeavePageHook();
        invalidateCachedPage();
        webDriver.navigate().refresh();
        T page = loadTopLevelPage(pageClass);
        setCachedPage(page);
        return page;
    }

    public void cleanSession() {
        webDriver.manage().deleteAllCookies();
    }

    @Nullable
    public abstract LogEntries getBrowserLogEntries();

    protected void setCommonWebBrowserCapabilities(DesiredCapabilities desiredCapabilities) {
        // If a required version is present, then set this as a desired capability. Only affects Remote browsers.
        Optional<String> browserVersion = getBrowserVersion();
        if (browserVersion.isPresent() && !browserVersion.get().isEmpty()) {
            desiredCapabilities.setCapability(CapabilityType.VERSION, browserVersion.get());
        }

        // Set logging preferences.
        LoggingPreferences loggingPreferences = getLoggingPreferences();
        desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);

        // If a platform is specified, set this desired capability. Only affects Remote browsers.
        Optional<Platform> platform = getPlatform();
        if (platform.isPresent()) {
            desiredCapabilities.setPlatform(platform.get());
        }
    }
}

