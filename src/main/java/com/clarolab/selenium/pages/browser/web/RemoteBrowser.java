package com.clarolab.selenium.pages.browser.web;

import com.clarolab.selenium.pages.actions.SeleniumActions;
import com.clarolab.selenium.pages.exception.FrameworkWebDriverException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.logging.Level;

public class RemoteBrowser extends WebBrowser {
    protected WebBrowser delegate;
    protected String seleniumHubURL;
    private static final Logger logger = LoggerFactory.getLogger(RemoteBrowser.class);


    public RemoteBrowser(WebBrowser delegate, String seleniumHubURL) {
        super(delegate.getBaseTestUrl(),
                delegate.getTimeouts(),
                delegate.getWebDriverPath(),
                delegate.getBrowserBinaryPath(),
                delegate.getBrowserVersion(),
                delegate.getBrowserLocale(),
                delegate.getStartWindowWidth(),
                delegate.getStartWindowHeight(), delegate.getBrowserLogLevel(), delegate.getBrowserLogFile(), delegate.getPlatform());
        this.delegate = delegate;
        this.seleniumHubURL = seleniumHubURL;
    }

    @Override
    public WebBrowserType getBrowserType() {
        return delegate.getBrowserType();
    }

    @Override
    public DesiredCapabilities getDesiredCapabilities() {
        return delegate.getDesiredCapabilities();
    }

    @Override
    protected WebDriver createWebDriver() throws FrameworkWebDriverException {
        try {
            RemoteWebDriver driver = new RemoteWebDriver(new URL(seleniumHubURL), delegate.getDesiredCapabilities());
            Level level = getLogLevel();
            driver.setLogLevel(level);
            driver.setFileDetector(new LocalFileDetector()); // Allow to upload local files to remote webdriver
            // https://code.google.com/p/selenium/source/browse/java/client/src/org/openqa/selenium/remote/LocalFileDetector.java
            return driver;
        } catch (MalformedURLException e) {
            throw new FrameworkWebDriverException("Invalid Selenium Hub URL given: " + seleniumHubURL, e);
        }
    }

    @Override
    public SeleniumActions getActions() {
        SeleniumActions actions = delegate.getActions();
        actions.setBrowser(this);  //We are running remotely, so the Actions should use the RemoteBrowser and RemoteWebDriver
        return actions;
    }

    @Nullable
    public LogEntries getBrowserLogEntries() {
        if (delegate.getBrowserType() == WebBrowserType.IE) {
            logger.info("IE does not support getting Browser Logs remotely. Returning null from getBrowserLogEntries");
            return null;
        }
        try {
            if (webDriver == null) {
                logger.info("The web driver was null in getBrowserLogEntries. Returning null.");
                return null;
            }
            logger.debug("Getting the available log types from remote Selenium node...");
            Set<String> availableLogTypes = webDriver.manage().logs().getAvailableLogTypes();

            logger.debug("Found available log types: {}", String.valueOf(availableLogTypes));

            if (availableLogTypes == null || !availableLogTypes.contains(LogType.BROWSER)) {
                logger.info("{} log type not allowed. Returning null.", LogType.BROWSER);
                return null;
            }
            logger.debug("Fetching logs from remote server...");

            LogEntries logs = webDriver.manage().logs().get(LogType.BROWSER);

            logger.info("Success getting remote logs!");

            return logs;
        } catch (Exception e) {
            // If some error occurs making the HTTP request to get logs, just return null.
            logger.info("Error retrieving remote logs: " + e.getMessage());
            return null;
        }
    }

    @Override
    public File saveScreenshotToFile(String filename) {
        TakesScreenshot screenshotDriver;
        screenshotDriver = (TakesScreenshot) new Augmenter().augment(getWebDriver());
        File scrFile = screenshotDriver.getScreenshotAs(OutputType.FILE);
        // Now you can do whatever you need to do with it, for example copy somewhere
        File outFile = new File(filename);
        try {
            FileUtils.copyFile(scrFile, outFile);
        } catch (IOException e) {
            logger.error("Error saving screenshot!", e);
        }
        return outFile;
    }
}

