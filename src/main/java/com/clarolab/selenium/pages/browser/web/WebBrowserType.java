package com.clarolab.selenium.pages.browser.web;

public enum WebBrowserType {
    IE, CHROME, FIREFOX, SAFARI, MOBILE;

    public static WebBrowserType forName(String name) {
        for (WebBrowserType type: WebBrowserType.values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("WebBrowserType must be 'IE', 'CHROME', 'FIREFOX', 'SAFARI', or 'MOBILE'");
    }
}
