package com.clarolab.selenium.pages.actions;

import com.clarolab.selenium.pages.browser.web.FirefoxBrowser;

/**
 * Selenium Actions for Firefox Browser.
 *
 * Currently, this is the same as BaseSeleniumActions, as we don't have any need to implement anything differently
 * for Firefox.
 */
public class FirefoxSeleniumActions extends BaseSeleniumActions<FirefoxBrowser> {
    public FirefoxSeleniumActions(FirefoxBrowser browser) {
        super(browser);
    }
}
