package com.clarolab.selenium.pages.actions;

import com.clarolab.selenium.pages.browser.web.ChromeBrowser;

/**
 * Selenium Actions for Chrome Browser.
 *
 * Currently, this is the same as BaseSeleniumActions, as we don't have any need to implement anything differently
 * for Chrome.
 */
public class ChromeSeleniumActions extends BaseSeleniumActions<ChromeBrowser> {
    public ChromeSeleniumActions(ChromeBrowser browser) {
        super(browser);
    }
}
