package com.clarolab.selenium.pages.actions;

import com.clarolab.selenium.pages.browser.Browser;
import com.clarolab.selenium.pages.config.TimeoutType;
import com.clarolab.selenium.pages.config.TimeoutsConfig;
import com.clarolab.selenium.pages.exception.FrameworkWebDriverException;
import com.clarolab.selenium.pages.exception.SeleniumActionsException;
import com.clarolab.selenium.pages.pages.SubPage;
import com.clarolab.selenium.pages.pages.TopLevelPage;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface SeleniumActions {

    void acceptAlert(TimeoutType timeout);

    WebElement clearText(By locator);

    WebElement clearText(WebElement el);

    WebElement click(By locator, TimeoutType timeout);

    WebElement click(WebElement el, TimeoutType timeout);

    <T extends SubPage> T clickAndLoadSubPage(By locatorToClick, Class<T> pageClass, TimeoutType timeout);

    <T extends SubPage> T clickAndLoadSubPage(WebElement el, Class<T> pageClass, TimeoutType timeout);

    <T extends TopLevelPage> T clickAndLoadTopLevelPage(By locatorToClick, Class<T> pageClass, TimeoutType timeout);

    <T extends TopLevelPage> T clickAndLoadTopLevelPage(WebElement el, Class<T> pageClass, TimeoutType timeout);

    void clickAndSelectFromList(By locatorToClick, By popoverLocator);

    void clickAndSelectFromList(WebElement clickable, By popoverLocator);

    void clickAndVerifyNotPresent(By locatorToClick, By locatorToVerifyNotPresent, TimeoutType timeout);

    void clickAndVerifyNotPresent(WebElement elToClick, By locatorToVerifyNotPresent, TimeoutType timeout);

    void clickAndVerifyNotVisible(By locatorToClick, By locatorToVerifyNotVisible, TimeoutType timeout);

    void clickAndVerifyNotVisible(WebElement elToClick, By locatorToVerifyNotVisible, TimeoutType timeout);

    WebElement clickAndVerifyPresent(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout);

    WebElement clickAndVerifyPresent(WebElement elToClick, By locatorToVerifyPresent, TimeoutType timeout);

    WebElement clickAndVerifySelected(By locatorToClick, TimeoutType timeout);

    WebElement clickAndVerifySelected(WebElement elToClick, TimeoutType timeout);

    WebElement clickAndVerifyNotSelected(By locatorToClick, TimeoutType timeout);

    WebElement clickAndVerifyNotSelected(WebElement elToClick, TimeoutType timeout);

    WebElement clickAndVerifyVisible(By locatorToClick, By locatorToVerifyVisible, TimeoutType timeout);

    WebElement clickAndVerifyVisible(WebElement elToClick, By locatorToVerifyVisible, TimeoutType timeout);

    WebElement clickNoWait(By locator) throws FrameworkWebDriverException;

    void dismissAlert(TimeoutType timeout);

    boolean doesElementHaveClass(By locator, String locatorClass);

    void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, String text, By popoverLocator,
                                                     String requiredPopupText);

    void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, int minChars, String text, By popoverLocator,
                                                     String requiredPopupText);

    Object executeJavascript(String script);

    boolean exists(By locator);

    boolean exists(By locator, WebElement parentEl);

    @Nullable
    WebElement findElementContainingChild(final By parentLocator, final By childLocator);

    @Nonnull
    WebElement findElementContainingChildWithWait(final By parentLocator, final By childLocator, TimeoutType timeout);

    @Nullable
    WebElement findElementContainingText(By locator, String text);

    @Nullable
    WebElement findElementContainingText(By locator, String text, boolean caseSensitive);

    @Nonnull
    WebElement findElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout);

    @Nonnull
    WebElement findElementContainingTextWithRefresh(final By locator, final String text, boolean caseSensitive, TimeoutType timeout);

    @Nonnull
    WebElement findElementContainingTextWithWait(final By locator, final String text, boolean caseSensitive, TimeoutType timeout);

    @Nonnull
    WebElement findElementContainingTextWithWait(By locator, String text, TimeoutType timeout);

    @Nonnull
    WebElement findElementWithRefresh(By locator, TimeoutType timeout);

    @Nonnull
    List<WebElement> findElementsContainingChild(final By parentLocator, final By childLocator);

    @Nonnull
    List<WebElement> findElementsContainingChildWithWait(final By parentLocator, final By childLocator, TimeoutType timeout);

    @Nullable
    WebElement findVisibleElement(By locator);

    @Nullable
    WebElement findVisibleElementContainingText(By locator, String text);

    @Nullable
    WebElement findVisibleElementContainingText(By locator, String text, boolean caseSensitive);

    @Nonnull
    WebElement findVisibleElementWithRefresh(By locator, TimeoutType timeout);

    @Nonnull
    WebElement findVisibleElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout);

    @Nonnull
    WebElement findVisibleElementWithWait(final By locator, TimeoutType timeout);

    @Nonnull
    WebElement findVisibleElementContainingTextWithWait(final By locator, final String text, TimeoutType timeout);

    @Nonnull
    List<WebElement> findVisibleElements(By locator);

    @Nonnull
    List<WebElement> findVisibleElementsContainingText(By locator, String text);

    @Nonnull
    List<WebElement> findVisibleElementsContainingText(By locator, String text, boolean caseSensitive);

    Actions getActionsBuilder();

    <B extends Browser> B getBrowser();

    void setBrowser(Browser browser);

    @Nullable
    WebElement getChildElement(By locator, WebElement parentEl);

    @Nonnull
    WebElement getChildElementWithWait(By locator, WebElement parentEl);

    @Nonnull
    List<WebElement> getChildElements(By locator, WebElement parentEl);

    String getCurrentURL();

    @Nullable
    WebElement getElement(By locator);

    @Nonnull
    WebElement getElementWithWait(By locator);

    List<WebElement> getElements(By locator);

    WebElement getParentElement(WebElement el);

    TimeoutsConfig getTimeoutsConfig();

    String getWebPageReadyState() throws Exception;

    WebElement inputText(By locator, String text);

    WebElement inputText(@Nonnull WebElement el, String text);

    WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator) throws SeleniumActionsException;

    WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator, int withRetryCount) throws SeleniumActionsException;

    WebElement inputTextSlowly(By locator, String text);

    WebElement inputTextSlowly(WebElement el, String text);

    WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator) throws SeleniumActionsException;

    WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator, int withRetryCount) throws SeleniumActionsException;

    void inputTinyMceText(String text);

    boolean isClickable(By locator);

    boolean isClickable(WebElement el);

    boolean isSelected(By locator);

    boolean isSelected(WebElement css);

    boolean isVisible(By locator);

    boolean isVisible(WebElement css);

    <T extends SubPage> T loadSubPage(Class<T> pageClass);

    <T extends TopLevelPage> T loadTopLevelPage(Class<T> pageClass);

    void scrollToTop();

    void scrollIntoView(By locator);

    void scrollIntoView(WebElement el);

    void scrollIntoView(By scrollContainerLocator, By locator);

    void scrollIntoView(By scrollContainerLocator, WebElement el);

    void verifyElementContainsText(By locator, String text, TimeoutType timeout);

    WebElement verifyElementDoesNotHaveClass(final By locator, final String locatorClass, TimeoutType timeout);

    WebElement verifyElementHasClass(By locator, String locatorClass, TimeoutType timeout);

    void verifyElementInvisible(By locator, TimeoutType timeout);

    void verifyElementNotPresented(By locator, TimeoutType timeout);

    WebElement verifyElementNotSelected(By locator, TimeoutType timeout);

    WebElement verifyElementNotSelected(WebElement el, TimeoutType timeout);

    WebElement verifyElementPresented(By locator, TimeoutType timeout);

    void verifyElementRemoved(WebElement element, TimeoutType timeout);

    WebElement verifyElementSelected(By locator, TimeoutType timeout);

    WebElement verifyElementSelected(WebElement el, TimeoutType timeout);

    WebElement verifyElementVisible(By locator, TimeoutType timeout);

    WebElement verifyAnyElementVisible(By locator, TimeoutType timeout);

    void verifyElementWithTextIsInvisible(By locator, String text, TimeoutType timeout);

    void verifyElementWithTextNotPresented(By locator, String text, TimeoutType timeout);

    WebElement verifyPageRefreshed(WebElement elementFromBeforeRefresh, By locatorAfterRefresh, TimeoutType timeout);

    void waitForJavascriptSymbolToBeDefined(String symbol, TimeoutType timeout);

    void waitForJavascriptSymbolToHaveValue(String symbol, String value, TimeoutType timeout);

    void waitForPageToBeStable(TimeoutType timeout);

    void waitForTinyMceToBeReady();

    void waitForWebPageReadyStateToBeComplete();

    <T> T waitOnExpectedCondition(ExpectedCondition<T> expectedCondition, String message, TimeoutType timeout);

    <T, V> V waitOnFunction(Function<T, V> function, T input, String message, TimeoutType timeout);

    <T extends TopLevelPage> T waitOnPagePredicateWithRefresh(Predicate<T> predicate, Class<T> pageClass, String message, TimeoutType timeout);

    /* Method to simplify general waiting code in Pages and Keywords. Takes a predicate and waits until it returns true.*/
    <T> void waitOnPredicate(Predicate<T> predicate, T input, String message, TimeoutType timeout);

    /* Same, but a helper for predicates that don't require an Input, because they use closure to interact with a containing class. */
    void waitOnPredicate(Predicate<Object> predicate, String message, TimeoutType timeout);

    /* Same, but refresh the page after each time the predicate is checked. */
    <T> void waitOnPredicateWithRefresh(Predicate<T> predicate, T input, String message, TimeoutType timeout);

    /* Same, but no input is required. */
    void waitOnPredicateWithRefresh(Predicate<Object> predicate, String message, TimeoutType timeout);

    WebElement waitUntilClickable(By locator, TimeoutType timeout);

    WebElement waitUntilClickable(WebElement el, TimeoutType timeout);

}
