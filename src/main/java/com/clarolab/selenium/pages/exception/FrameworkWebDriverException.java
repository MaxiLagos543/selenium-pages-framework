package com.clarolab.selenium.pages.exception;

public class FrameworkWebDriverException extends Exception {
    public FrameworkWebDriverException(String msg) {
        super(msg);
    }

    public FrameworkWebDriverException(String msg, Exception e) {
        super(msg, e);
    }
}
