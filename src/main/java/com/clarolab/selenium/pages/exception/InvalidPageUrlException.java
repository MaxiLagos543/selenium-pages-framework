package com.clarolab.selenium.pages.exception;

public class InvalidPageUrlException extends RuntimeException {
    public InvalidPageUrlException(String msg) {
        super(msg);
    }

    public InvalidPageUrlException(String msg, Exception e) {
        super(msg, e);
    }
}
