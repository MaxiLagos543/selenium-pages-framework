package com.clarolab.selenium.pages.pages;

import com.clarolab.selenium.pages.actions.SeleniumActions;
import com.clarolab.selenium.pages.config.TimeoutType;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

public class BaseSubPage <S extends SeleniumActions> implements SubPage {

    private static Logger logger = LoggerFactory.getLogger(BaseSubPage.class);
    private static final PageUtils PAGE_UTILS = new PageUtils();
    protected S a;
    protected Page parent = null;

    public final void setParent(Page parent) {
        this.parent = parent;
    }

    public final Page getParent() {
        return this.parent;
    }

    public final boolean hasParent() {
        return this.parent != null;
    }

    public final S getActions() {
        return a;
    }

    public final void setActions(SeleniumActions actions) {
        this.a = (S) actions;
    }

    public void pageLoadHook() {
        PAGE_UTILS.defaultPageLoadHook(this, a, getPageReadyTimeout());
    }

    public TimeoutType getPageReadyTimeout() {
        return TimeoutType.PAGE_READY_TIMEOUT;
    }

    @Nullable
    public By getPageIdentifier() {
        return null;
    }

    @Nullable
    public By getPageContainer() {
        return null;
    }

    public final void initSubPages() {
        PAGE_UTILS.initSubPages(this, a);
    }

    @Override
    public final void refreshElements() {
        PageFactory.initElements(getActions().getBrowser().getWebDriver(), this);
        initSubPages();
        pageLoadHook();
    }

    @Override
    public final void refreshPage() {
        getActions().getBrowser().refreshPage();
        refreshElements();
    }
}
