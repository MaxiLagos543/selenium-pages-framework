package com.clarolab.selenium.pages.pages;

import com.clarolab.selenium.pages.actions.SeleniumActions;
import com.clarolab.selenium.pages.config.TimeoutType;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.List;
import java.util.Optional;

public class PageUtils {
    private static final Logger logger = LoggerFactory.getLogger(PageUtils.class);

    public Optional<String> getWebPagePathForClass(Class<? extends TopLevelPage> pageClass) {
        WebPagePath annotation = pageClass.getAnnotation(WebPagePath.class);
        if (annotation == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(annotation.path());
    }

    public void defaultPageLoadHook(Page page, SeleniumActions a) {
        By pageIdentifier = page.getPageIdentifier();
        if (pageIdentifier != null) {
            a.verifyElementPresented(pageIdentifier, TimeoutType.PAGE_LOAD_TIMEOUT);
        }
    }

    public void defaultPageLoadHook(Page page, SeleniumActions a, TimeoutType timeout) {
        By pageIdentifier = page.getPageIdentifier();
        if (pageIdentifier != null) {
            a.verifyElementPresented(pageIdentifier, timeout);
        }
    }

    public static List<Field> getAllSubpageFields(Class<?> type) {
        List<Field> subpageFields = Lists.newArrayList();
        for (Field field : type.getDeclaredFields()) {
            if (field.getAnnotation(SubPageField.class) != null) {
                if (SubPage.class.isAssignableFrom(field.getType())) {
                    subpageFields.add(field);
                } else {
                    logger.warn("Class {} has a field annotated with @SubPageField that isn't a SubPage type", type.getSimpleName());
                }
            }
        }

        if (type.getSuperclass() != null) {
            subpageFields.addAll(getAllSubpageFields(type.getSuperclass()));
        }

        return subpageFields;
    }

    public void initSubPages(Page page, SeleniumActions a) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(page);
        List<Field> fields = getAllSubpageFields(page.getClass());
        for (Field field : fields) {
            Class type = field.getType();
            if (!SubPage.class.isAssignableFrom(type)) {
                logger.warn("Invalid @SubPageField in class '%s'. Must be a subclass of SubPage.");
                continue;
            }

            SubPage subPage = PageFactory.initElements(a.getBrowser().getWebDriver(), (Class<? extends SubPage>) field.getType());
            subPage.setActions(a);
            subPage.setParent(page);
            subPage.pageLoadHook();
            subPage.initSubPages();

            //Set the subpage field
            try {
                field.setAccessible(true);
                field.set(page, subPage);
            } catch (IllegalAccessException ex) {
                logger.error("Error instantiating SubPage field: " + field, ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void initSubPagesWithoutPageLoadHooks(Page page, SeleniumActions a) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(page);
        List<Field> fields = getAllSubpageFields(page.getClass());
        for (Field field : fields) {
            Class type = field.getType();
            if (!SubPage.class.isAssignableFrom(type)) {
                logger.warn("Invalid @SubPageField in class '%s'. Must be a subclass of SubPage.");
                continue;
            }

            SubPage subPage = PageFactory.initElements(a.getBrowser().getWebDriver(), (Class<? extends SubPage>) field.getType());
            subPage.setActions(a);
            subPage.setParent(page);
            initSubPagesWithoutPageLoadHooks(subPage, a);

            //Set the subpage field
            try {
                field.setAccessible(true);
                field.set(page, subPage);
            } catch (IllegalAccessException ex) {
                logger.error("Error instantiating SubPage field: " + field, ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void runPageLoadHooksForSubPages(Page page, SeleniumActions a) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(page);
        List<Field> fields = getAllSubpageFields(page.getClass());
        for (Field field : fields) {
            Class type = field.getType();
            if (!SubPage.class.isAssignableFrom(type)) {
                logger.warn("Invalid @SubPageField in class '%s'. Must be a subclass of SubPage.");
                continue;
            }

            //Get the subpage field
            SubPage subPage;
            try {
                field.setAccessible(true);
                subPage = (SubPage) field.get(page);
            } catch (IllegalAccessException ex) {
                logger.error("Error instantiating SubPage field: " + field, ex);
                throw new RuntimeException(ex);
            }
            if (subPage != null) {
                subPage.pageLoadHook();
                runPageLoadHooksForSubPages(subPage, a);
            }
        }
    }

    public <T extends Page> T loadPageFromURL(URI absoluteURL, Class<T> pageClass, WebDriver driver, SeleniumActions actions) {
        Preconditions.checkNotNull(absoluteURL, "Error: URI provided cannot be null in PageUtils#loadPageFromURL");
        Preconditions.checkNotNull(pageClass);
        Preconditions.checkNotNull(driver);
        Preconditions.checkNotNull(actions);
        Preconditions.checkArgument(absoluteURL.isAbsolute(), "Error: must provide an absolute URL to PageUtils#loadPageFromURL");
        driver.get(absoluteURL.toString());
        return loadCurrentPage(pageClass, driver, actions);
    }

    public <T extends Page> T loadCurrentPage(Class<T> pageClass, WebDriver driver, SeleniumActions actions) {
        T page = PageFactory.initElements(driver, pageClass);
        page.setActions(actions);
        page.initSubPages();
        page.pageLoadHook();
        return page;
    }

    public <T extends Page> T loadCurrentPageWithoutPageLoadHook(Class<T> pageClass, WebDriver driver, SeleniumActions actions) {
        T page = PageFactory.initElements(driver, pageClass);
        page.setActions(actions);
        initSubPagesWithoutPageLoadHooks(page, actions);
        return page;
    }
}
