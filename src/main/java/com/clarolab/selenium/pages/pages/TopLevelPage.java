package com.clarolab.selenium.pages.pages;

import javax.annotation.Nonnull;

public interface TopLevelPage extends Page {

    @Nonnull
    String getWebPagePath();

    void leavePageHook();

    long getPageLoadTime();

}
