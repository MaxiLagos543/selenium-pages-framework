package com.clarolab.selenium.pages.pages;

import org.openqa.selenium.By;

public interface SubPage extends Page {

    void setParent(Page parent);

    Page getParent();

    boolean hasParent();

    By getPageContainer();

}
