package com.clarolab.selenium.pages.pages;

import com.clarolab.selenium.pages.actions.SeleniumActions;
import com.clarolab.selenium.pages.config.TimeoutType;
import org.openqa.selenium.By;

import javax.annotation.Nullable;

public interface Page {

    SeleniumActions getActions();

    void setActions(SeleniumActions actions);

    void pageLoadHook();

    TimeoutType getPageReadyTimeout();

    @Nullable
    By getPageIdentifier();

    void initSubPages();

    void refreshElements();

    void refreshPage();

}
